# OWASP Juice Shop Application - Secure CI/CD Pipeline with DevSecOps Practices

This repository contains the OWASP Juice Shop application, an intentionally insecure web application designed for security training and awareness purposes. Juice Shop provides a controlled environment where users can learn about web application security by exploiting common vulnerabilities. In this project, additional security measures and DevSecOps practices have been integrated into the CI/CD pipeline to automate security scanning, vulnerability management, and secure deployments.

## Key Features:

**- OWASP Top 10 Vulnerabilities:** Practice finding and exploiting vulnerabilities like those listed in the OWASP Top 10.

**- Security Training:** Ideal for security professionals, developers, and students to gain hands-on experience in penetration testing and secure coding.

**- Challenges and Scoreboard:** Complete challenges at various difficulty levels, track your progress, and improve your skills.

**- Modern Web Technologies:** Built with Node.js and Angular to simulate real-world web applications.

**DevSecOps and Security Enhancements:**

This project also incorporates advanced security tools and practices through a secure CI/CD pipeline managed by GitLab. Each feature branch introduces additional security features and checks to enhance the overall security of the pipeline.

## Security Scanning Tools Integrated:

**- SAST (Static Application Security Testing):** Detect security issues in the source code using tools like Semgrep and njsscan. GitLeaks is used for detecting hardcoded secrets.

**- Software Composition Analysis (SCA):** Identify vulnerabilities in third-party libraries with retire.js.

**- DAST (Dynamic Application Security Testing):** Test the running application for vulnerabilities using Zed Attack Proxy (ZAP).

**- Image Scanning Tool:** Trivy is implemented to scan Docker images for vulnerabilities and misconfigurations after each pipeline run.

**Continuous Image Scanning:**

- ECR (Elastic Container Registry) is used for storing Docker images, with continuous security scanning enabled for vulnerabilities in container images.

**Vulnerability Management:**

Vulnerabilities detected through the automated security tools are tracked and managed using DefectDojo, providing visibility and traceability of security issues.

## Usage:

From Sources

1. Install node.js

2. Run git clone https://github.com/juice-shop/juice-shop.git --depth 1 (or
clone your own fork of the repository)

3. Go into the cloned folder with cd juice-shop

4. Run npm install (only has to be done before first start or when you change the source code)

5. Run npm start

6. Browse to http://localhost:3000


**Docker Container**

1. Install Docker

2. Run **docker pull bkimminich/juice-shop**

3. Run **docker run --rm -p 3000:3000 bkimminich/juice-shop**

4. Browse to **http://localhost:3000** (on macOS and Windows browse to
**http://192.168.99.100:3000** if you are using docker-machine instead of the native docker installation)